FROM ubuntu
MAINTAINER Tomas Markauskas <tomas@markauskas.lt>

RUN mkdir -p /app
ADD . /app/
WORKDIR /app
